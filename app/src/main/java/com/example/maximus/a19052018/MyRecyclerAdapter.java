package com.example.maximus.a19052018;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<User> users;
    private OnRecyclItemClick onRecyclItemClick;

    public interface OnRecyclItemClick {
        void onClick(User user);
    }

    public MyRecyclerAdapter(Context context, List<User> users, OnRecyclItemClick onRecyclItemClick) {
        this.context = context;
        this.users = users;
        this.onRecyclItemClick = onRecyclItemClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        holder.icon.setImageResource(user.getIcon());
        holder.userName.setText(user.getName());
        holder.phone.setText(user.getPhone());


    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView userName;
        TextView phone;

        public ViewHolder(View item) {
            super(item);
            icon = (ImageView) item.findViewById(R.id.icon);
            userName = (TextView) item.findViewById(R.id.user_name);
            phone = (TextView) item.findViewById(R.id.phone);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClick.onClick(users.get(getAdapterPosition()));
                }

            });

        }
    }
}
