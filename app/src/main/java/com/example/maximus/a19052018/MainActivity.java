package com.example.maximus.a19052018;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements MyRecyclerAdapter.OnRecyclItemClick {






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView listView = (RecyclerView) findViewById(R.id.list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(gridLayoutManager);
        listView.setAdapter(new MyRecyclerAdapter(this, generateUsers(),this));
    }

    public List<User> generateUsers() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(new User("Vasiy " + i,
                    "752 38 83" + (i % 10),
                    R.mipmap.ic_launcher,
                    "user" + i + "@gmail.com"));

        }
        return list;

    }

    @Override
    public void onClick(User user) {

        Toast.makeText(MainActivity.this,user.getPhone(),Toast.LENGTH_SHORT).show();


    }
}
