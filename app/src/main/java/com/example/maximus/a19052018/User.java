package com.example.maximus.a19052018;

public class User {
    private String name;
   private String phone;
   private int icon;
   private String email;

    public User(String name, String phone, int icon, String email) {
        this.name = name;
        this.phone = phone;
        this.icon = icon;
        this.email = email;

    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getIcon() {
        return icon;
    }

    public  String getEmail(){
        return email;
    }


}
