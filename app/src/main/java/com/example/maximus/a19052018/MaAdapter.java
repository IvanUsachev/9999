package com.example.maximus.a19052018;

import android.content.Context;
//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MaAdapter extends BaseAdapter {

    private Context context;
    private List<User> users;

    public MaAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_user, parent, false);
            holder = new
                    ViewHolder();
            holder.icon = (ImageView) rowView.findViewById(R.id.icon);
            holder.userName = (TextView) rowView.findViewById(R.id.user_name);
            holder.phone = (TextView) rowView.findViewById(R.id.phone);
            holder.email = (TextView)rowView.findViewById(R.id.iemal);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.icon.setImageResource(getItem(position).getIcon());
        holder.userName.setText(getItem(position).getName());
        holder.phone.setText(getItem(position).getPhone());
        holder.email.setText(getItem(position).getEmail());

        return rowView;
    }

    private static class ViewHolder {
        TextView email;
        ImageView icon;
        TextView userName;
        TextView phone;







    }

}









